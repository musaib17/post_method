const express = require("express");
const app = express();
app.use(express.static("views"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
const { check, validationResult } = require('express-validator');
const port = process.env.PORT || 3000;

app.get('/', function (req, res) {
    res.sendFile('index.html');

});

app.get('*', function (req, res) {
    res.sendFile(__dirname + '/views/404.html')
})

app.post('/registered', [check('firstname', 'Firstname is not valid').exists().isLength({ min: 1 }),
check('lastname', 'Lastname is not valid').exists().isLength({ min: 1 }),
check('email',).isEmail().normalizeEmail().withMessage({ code: 123, message: "Invalid email" }),
check('password',).isLength({ min: 6 }).withMessage({ code: 123, message: "password should be minimum 6 character long" })], (req, res) => {
    const errors = validationResult(req);
    const error = errors.array()
    if (!errors.isEmpty()) {
     for (var err of error){
            console.log(err.msg);
            return res.status(400).json({
                success: false,
                error: err.msg
            })
        }
        


    }
    else {
        const response = {
            first_name: req.body.firstname,
            last_name: req.body.lastname,
            Gender: req.body.gender,
            Email: req.body.email,
            password: req.body.password,
            newsletter: Boolean(req.body.newsletter),
        };
        return res.status(200).send({
            success: true,
            data: response,
        });
    }
});

app.listen(port, () => {
    console.log("Is running on http://localhost:${port}");
})